import java.io.Serializable;

public class Transporter extends Vehicle implements Serializable {

	private int seats;
	private int loadarea;

	public Transporter(String manufacturer2, int year2, String name2, String licenseplate2, int seats2, int loadarea2) {
		super(manufacturer2, "Transporter", year2, name2, licenseplate2);
		seats = seats2;
		loadarea = loadarea2;
	}

	public Transporter() {}

	public int getSeats() {
		return seats;
	}

	public int getLoadarea() {
		return loadarea;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public void setLoadarea(int loadarea) {
		this.loadarea = loadarea;
	}
	
	
}
