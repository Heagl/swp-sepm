import java.util.Scanner;

public class main {

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		VehicleLotView vlv= new VehicleLotView();
		/*
		vlv.add_new_Vehicle(new Car("Audi", 2000, "A3", "IL-GB 502", "Left bumper broken"));
		vlv.add_new_Vehicle(new Car("VW", 1940, "Kaefer", "I-BestCar01", "its just broken down completly"));
		vlv.add_new_Vehicle(new Car("BMW", 1998, "", "IL-GB 502", "Large as Hell"));
		vlv.add_new_Vehicle(new Motorbike("Kawasaki", 2017, "Ninja", "IL-GB 500", 1500));
		vlv.add_new_Vehicle(new Motorbike("Indian", 2017, "Scout", "IL-GB 499", 1500));
		vlv.add_new_Vehicle(new Transporter("VW", 2000, "Transporter", "IL-GB 498", 10,10));
		*/
		vlv.load();
		
		
		System.out.println("All of my commands:");
		System.out.println("rC for rentCar");
		System.out.println("rM for rentMotorbike");
		System.out.println("rT for rentTransporter");
		System.out.println("returnC for returnCar");
		System.out.println("returnM for returnMotorbike");
		System.out.println("returnT for returnTransporter");
		while(true)
		{
			
			String rs = sc.nextLine();
			if(rs.equals("rC"))
			{
				vlv.rent_a_Car();
			}
			else if(rs.equals("rM"))
			{
				vlv.rent_a_Motorbike();
			}
			else if(rs.equals("rT"))
			{
				vlv.rent_a_Transporter();
			}
			else if(rs.equals("returnM"))
			{
				String lp = sc.nextLine();
				vlv.return_a_Motorbike(lp);
			}
			else if(rs.equals("returnC"))
			{
				String lp = sc.nextLine();
				vlv.return_a_Car(lp);
			}
			else if(rs.equals("returnT"))
			{
				String lp = sc.nextLine();
				vlv.return_a_Transporter(lp);
			}
		}
		
	
	
	
	}

}
