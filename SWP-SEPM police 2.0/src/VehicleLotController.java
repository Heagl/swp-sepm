import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class VehicleLotController implements Serializable {

	ArrayList <Car> cars = new ArrayList<Car>();
	ArrayList <Motorbike> bikes = new ArrayList<Motorbike>();
	ArrayList <Transporter> transporters = new ArrayList<Transporter>();
	
	public String rentCar()
	{
		for(int i= 0; i<cars.size(); i++)
		{
			
			if(!cars.get(i).getRented())
			{
				cars.get(i).setRented(true);
				return cars.get(i).getLicenseplate();
			}
		}
		return null;
	}
	public String rentMotorbike()
	{
		for(int i= 0; i<bikes.size(); i++)
		{
			
			if(!bikes.get(i).getRented())
			{
				bikes.get(i).setRented(true);
				return bikes.get(i).getLicenseplate();
			}
		}
		return null;
	}
	public String rentTransporter()
	{
		for(int i= 0; i<transporters.size(); i++)
		{
			
			if(!transporters.get(i).getRented())
			{
				transporters.get(i).setRented(true);
				return transporters.get(i).getLicenseplate();
			}
		}
		return null;
	}
	
	public void addCar(Car v)
	{
		cars.add(v);
	}
	public void addTransporter(Transporter t)
	{
		transporters.add(t);
	}
	public void addMotorbike( Motorbike m)
	{
		bikes.add(m);
	}
	public boolean returnTransporter(String Licenseplate)
	{
		for(int i= 0; i<transporters.size(); i++)
		{
			
			if((Licenseplate.equals(transporters.get(i).getLicenseplate()))&&(transporters.get(i).getRented()))
			{
				transporters.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnMotorbike(String Licenseplate)
	{
		for(int i= 0; i<bikes.size(); i++)
		{
			
			if((Licenseplate.equals(bikes.get(i).getLicenseplate()))&&(bikes.get(i).getRented()))
			{
				bikes.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnCar(String Licenseplate)
	{
		for(int i= 0; i<cars.size(); i++)
		{
			
			if((Licenseplate.equals(cars.get(i).getLicenseplate()))&&(cars.get(i).getRented()))
			{
				cars.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	
	
	
	public ArrayList<Car> getCars() {
		return cars;
	}
	public void setCars(ArrayList<Car> cars) {
		this.cars = cars;
	}
	public ArrayList<Motorbike> getBikes() {
		return bikes;
	}
	public void setBikes(ArrayList<Motorbike> bikes) {
		this.bikes = bikes;
	}
	public ArrayList<Transporter> getTransporters() {
		return transporters;
	}
	public void setTransporters(ArrayList<Transporter> transporters) {
		this.transporters = transporters;
	}
	
	

}
