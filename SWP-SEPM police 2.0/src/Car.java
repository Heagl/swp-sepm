import java.io.Serializable;

public class Car extends Vehicle implements Serializable{
	
	private String extras;
	public Car(String manufacturer2, int year2,String name2, String licenseplate2, String extras2) {
		super(manufacturer2, "Car", year2,name2, licenseplate2);
		extras = extras2;
	}

	public Car() {}

	public String getExtras()
	{
		return this.extras;
	}
	public void setExtras(String extras){
		this.extras = extras;
	}

}
