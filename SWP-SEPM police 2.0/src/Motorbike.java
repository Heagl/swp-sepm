import java.io.Serializable;

public class Motorbike extends Vehicle implements Serializable {

	private int ccm;
	public Motorbike(String manufacturer2, int year2,String name2, String licenseplate2, int ccm2) {
		super(manufacturer2, "motorbike", year2, name2, licenseplate2);
		ccm = ccm2;
	}
	public Motorbike() {}
	public int getCcm() {
		return ccm;
	}
	public void setCcm(int ccm) {
		this.ccm = ccm;
	}
	
}
