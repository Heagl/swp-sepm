import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class VehicleLotView{
	Car c = new Car();
	Motorbike m = new Motorbike();
	Transporter t = new Transporter();
	VehicleLotController vlc = new VehicleLotController();
	String Licenseplate;

	public void add_new_Vehicle(Object v)
	{

		if(v.getClass().equals(c.getClass()))
		{
			vlc.addCar((Car) v);
		}
		else if(v.getClass().equals(m.getClass()))
		{
			vlc.addMotorbike((Motorbike) v);
		}
		else if(v.getClass().equals(t.getClass()))
		{
			vlc.addTransporter((Transporter) v);
		}
	}
	public void rent_a_Car()
	{
		Licenseplate = null;
		Licenseplate = vlc.rentCar();
		if(Licenseplate != null)
		{
			System.out.println("The Vehicle "+Licenseplate+" was rented.");
		}
		else
		{
			System.out.println("We are sorry, but there are no Cars available at the moment.");
		}
	}
	public void rent_a_Motorbike()
	{
		Licenseplate = null;
		Licenseplate = vlc.rentMotorbike();
		if(Licenseplate != null)
		{
			System.out.println("The Vehicle  "+Licenseplate+" was rented.");
		}
		else
		{
			System.out.println("We are sorry, but there are no Motorbikes available at the moment.");
		}
	}
	public void rent_a_Transporter()
	{
		Licenseplate = null;
		Licenseplate = vlc.rentTransporter();
		if(Licenseplate != null)
		{
			System.out.println("The Vehicle "+Licenseplate+" was rented.");
		}
		else
		{
			System.out.println("We are sorry, but there are no Transporters available at the moment.");
		}
	}
	public void return_a_Car(String Licenseplate)
	{
		if(vlc.returnCar(Licenseplate))
		{
			System.out.println("The Vehicle with the Licenseplate "+Licenseplate+" was succesfully returned.");
		}
		else
		{
			System.out.println("The Vehicle isnt registered.");
		}

	}
	public void return_a_Motorbike(String Licenseplate)
	{
		if(vlc.returnMotorbike(Licenseplate))
		{
			System.out.println("The Vehicle with the Licenseplate "+Licenseplate+" was succesfully returned.");
		}
		else
		{
			System.out.println("The Vehicle isnt registered.");
		}
	}
	public void return_a_Transporter(String Licenseplate)
	{
		if(vlc.returnTransporter(Licenseplate))
		{
			System.out.println("The Vehicle with the Licenseplate "+Licenseplate+" was succesfully returned.");
		}
		else
		{
			System.out.println("The Vehicle isnt registered.");
		}
	}

	public void save()
	{
		FileOutputStream fos1;
		try {
			fos1 = new FileOutputStream("d:\\ser.xml");
			java.beans.XMLEncoder xe1 = new java.beans.XMLEncoder(new BufferedOutputStream(fos1));
			xe1.writeObject(vlc.bikes);
			xe1.writeObject(vlc.cars);
			xe1.writeObject(vlc.transporters);
			xe1.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}

	public void load()
	{
		try {
			FileInputStream fileIn = new FileInputStream("d:\\ser.xml");
			BufferedInputStream in = new BufferedInputStream(fileIn);
			java.beans.XMLDecoder dec = new java.beans.XMLDecoder(in);
			vlc.setBikes((ArrayList<Motorbike>) dec.readObject());
			vlc.setCars((ArrayList<Car>) dec.readObject());
			vlc.setTransporters((ArrayList<Transporter>) dec.readObject());
			in.close();
			fileIn.close();
		}
		catch(IOException i) {
			System.out.println(i.getMessage());
		}
	}
}
