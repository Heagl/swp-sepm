import java.io.Serializable;

public class Vehicle implements Serializable {
	private String type;
	private String name;
	private String licenseplate;
	private String manufacturer;
	private int year;
	private boolean rented;

	
	public Vehicle(String manufacturer2, String type2 , int year2,String name2, String licenseplate2)
	{
		type = type2;
		year = year2;
		manufacturer = manufacturer2;
		rented = false;
		licenseplate = licenseplate2;
		name = name2;
	}

	public Vehicle() {}
	
	public boolean getRented()
	{
		return this.rented;
	}
	public void setRented(boolean rented)
	{
		this.rented = rented;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setLicenseplate(String licenseplate) {
		this.licenseplate = licenseplate;
	}

	public String getLicenseplate(){
		return licenseplate;
	}
}
