
public class Vehicle {

	int year;
	String manufacturer;
	String licenceplate;
	String vehicletype;
	boolean rented;
	String name;
	
	public Vehicle(String n,String m, String l, String t, int y)
	{
		year = y;
		manufacturer = m;
		licenceplate = l;
		vehicletype = t;
		name = n;
	}
	public void getBorrowed()
	{
		if(rented){
			System.out.println("The Vehicle "+ name +" is currently rented.");
		}
		else
		{
			System.out.println("The Vehicle "+ name +" is currently not rented.");
		}
	}

}
