import java.util.ArrayList;

public class VehicleLot {
		
	ArrayList <Car> cars = new ArrayList();
	ArrayList <Motorbike> bikes = new ArrayList();
	ArrayList <Transporter> transporters = new ArrayList();
	public String rentCar()
	{
		for(int i= 0; i<cars.size(); i++)
		{
			
			if(!cars.get(i).rented)
			{
				System.out.println("The Vehicle "+ cars.get(i).name +" was succesfully rented.");
				cars.get(i).rented = true;
				return cars.get(i).name;
			}
		}
		System.out.println("I am very sorry but it seems like we dont have any car here at the moment. Please come back later.");
		return null;
	}
	public String rentBike()
	{
		for(int i= 0; i<bikes.size(); i++)
		{
			
			if(!bikes.get(i).rented)
			{
				System.out.println("The Vehicle "+ bikes.get(i).name +" was succesfully rented.");
				bikes.get(i).rented = true;
				return bikes.get(i).name;
			}
		}
		System.out.println("I am very sorry but it seems like we dont have any Motorbike here at the moment. Please come back later.");
		return null;
	}
	public String rentTrasporter()
	{
		for(int i= 0; i<transporters.size(); i++)
		{
			
			if(!transporters.get(i).rented)
			{
				System.out.println("The Vehicle "+ transporters.get(i).name +" was succesfully rented.");
				transporters.get(i).rented= true;
				return transporters.get(i).name;
			}
		}
		System.out.println("I am very sorry but it seems like we dont have any Transporter here at the moment. Please come back later.");
		return null;
	}
	public void addCar(Car c)
	{
		cars.add(c);
	}
	public void addTransporter(Transporter t)
	{
		transporters.add(t);
	}
	public void addMotorbike( Motorbike m)
	{
		bikes.add(m);
	}
	
	public void returnTransporter(String n)
	{
		for(int i= 0; i<transporters.size(); i++)
		{
			
			if((n == transporters.get(i).name)&&(transporters.get(i).rented))
			{
				System.out.println("The Vehicle "+ transporters.get(i).name +" was succesfully returned.");
				transporters.get(i).rented= false;
			}
		}		
	}
	public void returnCar(String n)
	{
		for(int i= 0; i<cars.size(); i++)
		{
			
			if((n == cars.get(i).name)&&(cars.get(i).rented))
			{
				System.out.println("The Vehicle "+ cars.get(i).name +" was succesfully returned.");
				cars.get(i).rented= false;
			}
		}	
	}
	public void returnMotorbike(String n)
	{
		for(int i= 0; i<bikes.size(); i++)
		{
			
			if((n == bikes.get(i).name)&&(bikes.get(i).rented))
			{
				System.out.println("The Vehicle "+ bikes.get(i).name +" was succesfully returned.");
				bikes.get(i).rented= false;
			}
		}	
	}
}