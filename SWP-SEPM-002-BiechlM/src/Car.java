
public class Car extends Vehicle {

	private String extras;
	
	public Car(String n,String m, String l, String e, int y)
	{
		super(n,m,l,"Car",y);
		extras = e;
	}
}
