
public class Main {

	public static void main(String[] args) {
		VehicleLot lot = new VehicleLot();

		Car c1 = new Car("Car1","Volkswagen", "BP 312 ABC", "CD-Player", 2007);
		Car c2 = new Car("Car2","BMW", "BP 123 CAB", "Bluetooth", 1999);
		Car c3 = new Car("Car3","Hyundai", "BP 321 BAC", "TomTom", 2000);
		Car c4 = new Car("Car4","Porsche", "BP 213 CBA", "Radio", 2009);

		lot.addCar(c1);
		lot.addCar(c2);
		lot.addCar(c3);
		lot.addCar(c4);

		Transporter t1 = new Transporter("Transporter1","Toyota","BP 002 ACD", 2000, 6, 10);
		Transporter t2 = new Transporter("Transporter2","Renault","BP 020 ADC", 2010, 9, 15);

		lot.addTransporter(t1);
		lot.addTransporter(t2);

		Motorbike m1 = new Motorbike("Motorbike1","Kawasaki", "BP 300 ACE",2016, 1200 );
		Motorbike m2 = new Motorbike("Motorbike2","Kawasaki", "BP 003 BAD",2015, 1100 );
		Motorbike m3 = new Motorbike("Motorbike3","BMW", "BP 033 ACD",2010, 1000 );
		Motorbike m4 = new Motorbike("Motorbike4","Honda", "BP 303 CAD",2000, 950 );
		Motorbike m5 = new Motorbike("Motorbike5","Yamaha", "BP 023 BCA",2008, 500 );
		Motorbike m6 = new Motorbike("Motorbike6","KTM", "BP 233 ACB",2011, 250 );
		Motorbike m7 = new Motorbike("Motorbike7","KTM", "BP 203 ADW",2012, 400 );

		lot.addMotorbike(m1);
		lot.addMotorbike(m2);
		lot.addMotorbike(m3);
		lot.addMotorbike(m4);
		lot.addMotorbike(m5);
		lot.addMotorbike(m6);
		lot.addMotorbike(m7);

		lot.rentCar();
		lot.rentBike();
		lot.rentTrasporter();
		lot.rentTrasporter();
		lot.rentTrasporter();

		lot.returnCar("Car1");
		lot.returnMotorbike("Motorbike1");
		lot.returnTransporter("Transporter1");
		lot.returnTransporter("Transporter2");
		lot.rentCar();
	}
	
	

}
